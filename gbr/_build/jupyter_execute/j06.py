#!/usr/bin/env python
# coding: utf-8

# #  Statistical Downscaling Method

# In[1]:


# basic
import sys
import os

# common
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from sklearn.cluster import KMeans
import pickle

import os
import conda
conda_file_dir = conda.__file__
conda_dir = conda_file_dir.split('lib')[0]
proj_lib = os.path.join(os.path.join(conda_dir, 'share'), 'proj')
os.environ["PROJ_LIB"] = proj_lib
from mpl_toolkits.basemap import Basemap

#lib
from lib.calibration import *
from lib.predictor_definition_building import *
from lib.PCA_predictor_sst_mld import PCA_EstelaPred_sea_mask, Plot_EOFs_EstelaPred, standardise_predictor_prep,plot_graph_variance_PCA,plot_PCs
from lib.mda import Normalize, DeNormalize
from lib.plots_kma import plot_3D_kmeans, plot_scatter_kmeans, plot_grid_kmeans
from lib.plots_base import basemap_ibtracs, basemap_var, basemap_scatter, axplot_basemap, basemap_scatter_both
from lib.plots_dwts import colorp, custom_colorp, Plot_DWTs_Mean_Anom, Plot_DWTs_totalmean,Plot_Probs_WT_WT, Plot_Probs_WT_WT_anomaly, Plot_Probs_WT_WT_WT, Plot_DWTs_Probs, Report_Sim_oneyear, Report_Sim, Plot_DWTs_counts, Chrono_dwts_tcs, Chrono_probs_tcs, Plot_dwts_colormap
from lib.plots_tcs import get_storm_color, get_category, Plot_DWTs_tracks, get_category_value
from lib.plots_aux import data_to_discret, colors_dwt
from lib.extract_tcs import Extract_Rectangle, dwt_tcs_count, dwt_tcs_count_tracks, dwt_tcs_count_tracks_pmin,apply_factor

import warnings
warnings.filterwarnings('ignore')
from IPython.display import Image


# <br />
# <br />
# 
# ## Daily Weather Types (DWTs) classification
# 

# **A weather type approach is proposed.**<br>
# **The index predictor is first partioned into a certain number of clusters, DWTS, obtained combining three data mining techniques.**

# 
# 
# ### Principal Component Analysis (PCA)
# 

# **The PCA is employed to reduce the high dimensionality of the original data space and thus simplify the classification process, transforming the predictor fields into spatial and temporal modes.**

# In[2]:


path_p = r'/home/administrador/Documentos/seasonal/seasonal_forecast/new/'
xs = xr.open_dataset(path_p+'xs_index_vars_19822019_2deg_new_CR_ncep.nc')
xs_trmm = xs_trmm = xr.open_dataset(path_p+'xs_trmm_1982_2019_2deg_new_CR.nc')
df = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR.pkl')#historic


# In[3]:


# predictor area
lo_area = [140, 163]
la_area = [-26,-9]


# In[3]:


# PCA parameters
ipca, xds_PCA = PCA_EstelaPred_sea_mask(xs, ['index']) #ipca son las componentes principales, las 416 que salen
xds_PCA


# <br>
# 
# **PCA projects the original data on a new space searching for the maximum variance of the sample data.**<br>
# **The first 43 PCs are captured, which explain the 90 % of the variability as shown:**

# In[4]:


variance_PCA = plot_graph_variance_PCA(xds_PCA)


# <br>
# 
# **The eigenvectors (the empirical orthogonal functions, EOFs) of the data covariance matrix define the vectors of the new space.They represent the spatial variability.**

# In[6]:


fig_eofs = Plot_EOFs_EstelaPred(xds_PCA, 4);


# <br>
# 
# **The PCs represent the temporal variability.**

# In[6]:


fig_PCA = plot_PCs(xds_PCA,4)


# <br><br>
# 
# ### K-means clustering

# **Daily synoptic patterns of the index predictor are obtained using the K-means clustering algorithm. It divides the data space into N = 49 clusters, a number which must be a compromise between an easy handle characterization of the synoptic patterns and the best reproduction of the variability in the data space. Previous works with similar analysis confirmed that the selection of this number is adequate (Rueda et al. 2017).**
# <br>
# 
# **Each cluster is defined by a prototype and formed by the data for which the prototype is the nearest.**
# 
# **Finally the best match unit (bmus) of daily clusters are reordered into a lattice following a geometric criteria, so that similar clusters are placed next to each other for a more intuitive visualization.**

# In[7]:


# PCA data
variance = xds_PCA.variance.values[:]
EOFs = xds_PCA.EOFs.values[:]
PCs = xds_PCA.PCs.values[:]

var_anom_std = xds_PCA.pred_std.values[:]
var_anom_mean = xds_PCA.pred_mean.values[:]
time = xds_PCA.time.values[:]

variance = xds_PCA.variance.values
percent = variance / np.sum(variance)*100
percent_ac = np.cumsum(percent)
n_comp_95 = np.where(percent_ac >= 95)[0][0]
n_comp_90 = np.where(percent_ac >= 90)[0][0]
    
# plot
n_comp = n_comp_90

nterm = n_comp_90 + 1 #n_comp_90 es el número de PC que explican el 90% de la varianza, que en este caso son 237
PCsub = PCs[:, :nterm]
EOFsub = EOFs[:nterm, :]

# normalization
data = PCsub
data_std = np.std(data, axis=0)
data_mean = np.mean(data, axis=0)

#normalize but keep PCs weigth
data_norm = np.ones(data.shape)*np.nan
for i in range(PCsub.shape[1]):
    data_norm[:,i] = np.divide(data[:,i] - data_mean[i], data_std[0]) #si no usas la desviación estándar del primero da mucho error

#KMEANS
num_clusters = 49
kma = KMeans(n_clusters=num_clusters, n_init=1000).fit(data_norm)
kma

#store
with open(r'/home/administrador/Documentos/seasonal/seasonal_forecast/new/kma_model/kma_index_okb_CR_ncepP.pkl', "wb") as f:
    pickle.dump(kma, f)
    
#a measure of the error of the algorithm
kma.inertia_
# In[9]:


path_kma = path_p+'kma_model/kma_index_okb_CR.pkl'
xds_kma_ord,xds_kma = func_kma_order(path_kma,xds_PCA,xs)


# In[6]:


xds_kma = xr.open_dataset(path_p+'kma_model/xds_kma_index_vars_1b_CR.nc')
xds_kma_ord = xr.open_dataset(path_p+'xds_kma_ord_CR.nc')


# In[11]:


xds_kma.bmus.values


# In[66]:


xds_kma_sel = trmm_kma(xds_kma,xs_trmm)


# <br>
# 
# **The resulting classification can be seen in the first three PCs space of the predictor index data. The obtained centroids (black dots), span the wide variability of the data.**

# In[12]:


# CHECKUP PLOT 3D
get_ipython().run_line_magic('matplotlib', 'inline')
fig = plot_3D_kmeans(xds_kma_ord, xds_kma_ord.bmus.values, xds_kma_ord.cenEOFs.values, size_l=12, size_h=10);


# **Index predictor data and the obtained centroids in the first 10 PCs space.**

# In[13]:


fig = plot_grid_kmeans(xds_kma_ord, xds_kma_ord.bmus.values, xds_kma_ord.cenEOFs.values, 9, ibmus=range(49), size_l=20, size_h=18);


# <br />
# 
# ### DWTs plotting

# In[12]:


path_st = r'/home/administrador/Documentos/'
xds_ibtracs, xds_SP = storms_sp(path_st)


# In[13]:


st_bmus,st_lons,st_lats, st_categ = st_bmus(xds_SP,xds_kma)


# In[14]:


# custom colorbar for index
color_ls = ['white','cyan','cornflowerblue','darkseagreen','olivedrab','gold','darkorange','orangered','red','deeppink','violet','darkorchid','purple','midnightblue']
custom_cmap = custom_colorp(color_ls)

# custom colorbar for index
color_cat = ['green','yellow','darkorange','red','purple','black']
cat_cmap = custom_colorp(color_cat)


# <br>
# 
# **DWTs lattice and corresponding colors:**

# In[18]:


fig_dwt_lattice = Plot_dwts_colormap(xds_kma.n_clusters.size)


# <br>
# 
# **The resulting clustering of the index predictor -> the DWTs, each cell is the mean of all the patterns of that corresponding cluster:**

# In[20]:


fig = Plot_DWTs_Mean_Anom(16,13,xds_kma, xs, ['index'], minis=[0], maxis=[.85],levels=[len(color_ls)], kind='mean',cmap = [custom_cmap],cfill='grey',genesis='on', st_bmus=st_bmus, 
                          st_lons=st_lons, st_lats=st_lats, markercol='white', markeredge='k');


# <br>
# 
# **DWTs (index) total mean:**

# In[19]:


fig = Plot_DWTs_totalmean(xs, ['index'], minis=[0], maxis=[.85],levels=[len(color_ls)],cmap=[custom_cmap],cfill='grey');


# <br>
# 
# **DWTs (index) Anomalies:**

# In[17]:


fig = Plot_DWTs_Mean_Anom(16,13,xds_kma, xs, ['index'], minis=[-.5], maxis=[.5], levels=[20], kind='anom', cmap=['RdBu_r'], 
                          genesis='on', cfill='grey',st_bmus=st_bmus, st_lons=st_lons, st_lats=st_lats, markercol='mediumspringgreen', markeredge='k');


# <br />
# <br />
# 
# ### Cluster homegeinity

# **The DWTs are different to each other, showing the high variability of the data space. The clusters are also very homogenous inside. This confirms N = 49 as a good choice, which additionally is a manageable number.**

# In[11]:


fig_41 = plot_41(xs,xds_kma,267)


# In[22]:


fig_5 = plot_5(xs,xds_kma)


# <br />
# <br />
# 
# ### DWTs plotting with predictand variables

# In[14]:


fig = Plot_DWTs_Mean_Anom(16,13,xds_kma, xs, ['sst', 'dbss'], minis=[22, 0], maxis=[32, 200], levels=[(32-22)/0.5, 8], kind='mean', 
                          cmap=[colorp(), 'seismic'], genesis='on', cfill='grey',st_bmus=st_bmus, st_lons=st_lons, st_lats=st_lats, markercol='white', markeredge='k');


# <br>
# 
# **DWTs - SST and MLD total mean:**

# In[15]:


fig = Plot_DWTs_totalmean(xs, ['sst','dbss'], minis=[22,0], maxis=[32,200], levels=[(32-22)/0.5,20],  cmap=[colorp(),'seismic'],cfill='grey');


# <br>
# <br>
# 
# **DWTs - SST and MLD Anomalies:**

# In[16]:


fig = Plot_DWTs_Mean_Anom(16,13,xds_kma, xs, ['sst', 'dbss'], minis=[-4, -60], maxis=[4, 60], levels=[20, 6], kind='anom', cmap=['coolwarm', 'seismic'], 
                          genesis='on', cfill='grey',st_bmus=st_bmus, st_lons=st_lons, st_lats=st_lats, markercol='mediumspringgreen', markeredge='k');


# ```{important} 
# 
# Clear patterns can be extracted from these figures related to TCs genesis. Most of it takes place under the following conditions:
# 
# * **SST interval from 28ºC to 30ºC (specially 28.5 to 29.5 ºC) that correspond to positive or zero SST anomalies.**
# * **MLD values equal or smaller to 50 m that correspond to negative anomalies.**
# 
# ```

# <br />
# <br />
# 
# ## DWTs seasonality, annual variability and chronology

# **Several plots are shown to better analyse the distribution of DWTs, their transition, persistence and conditioning to TCs occurrence and to AWT.**

# In[16]:


path_mjo_awt = r'/home/administrador/Documentos/STORMS_VIEWER/'


# In[17]:


awt,mjo,awt0 = awt_mjo_ds(path_mjo_awt)


# <br>
# 
# **Seasonality:**

# In[18]:


bmus_DWT, bmus_time,awt0_sel_bmus,bmus_AWT,bmus_MJO = bmus_dwt_mjo(mjo,awt,awt0,xds_kma)


# In[19]:


fig = Plot_DWTs_Probs(bmus_DWT, bmus_time, 49, height=10, width=18);


# <br>
# 
# **Datasets from historical, synthetic and both TCs databases**

# In[20]:


df = pd.read_pickle(path_p+'df_pmin.pkl')
dfh = pd.read_pickle(path_p+'df_pmin_hist.pkl')
dfs = pd.read_pickle(path_p+'df_pmin_syn.pkl')


# **TC counting in cells of 3x3º for the target area for all the calibration period and for all the databases.**

# **All TC categories:**

# In[25]:


ds = dwt_tcs_count_tracks_pmin(xds_kma, df, dx=3, dy=3, lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
ds.to_netcdf(path_p+'xs_dwt_counts3_CR.nc')
dsh = dwt_tcs_count_tracks_pmin(xds_kma, dfh, dx=3, dy=3, lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
dsh.to_netcdf(path_p+'xs_dwt_counts3_CRh.nc')
dss = dwt_tcs_count_tracks_pmin(xds_kma, dfs, dx=3, dy=3, lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
dss.to_netcdf(path_p+'xs_dwt_counts3_CRs.nc')


# **TC category >= 3:**

# In[30]:


ds = dwt_tcs_count_tracks_pmin(xds_kma, df, dx=3, dy=3, categ=965,lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
ds.to_netcdf(path_p+'xs_dwt_counts3_965_CR.nc')
dsh = dwt_tcs_count_tracks_pmin(xds_kma, dfh, dx=3, dy=3, categ=965,lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
dsh.to_netcdf(path_p+'xs_dwt_counts3_965_CRh.nc')
dss = dwt_tcs_count_tracks_pmin(xds_kma, dfs, dx=3, dy=3, categ=965,lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
dss.to_netcdf(path_p+'xs_dwt_counts3_965_CRs.nc')


# **TC category >= 2:**

# In[31]:


ds = dwt_tcs_count_tracks_pmin(xds_kma, df, dx=3, dy=3, categ=979,lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
ds.to_netcdf(path_p+'xs_dwt_counts3_979_CR.nc')
dsh = dwt_tcs_count_tracks_pmin(xds_kma, dfh, dx=3, dy=3, categ=979,lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
dsh.to_netcdf(path_p+'xs_dwt_counts3_979_CRh.nc')
dss = dwt_tcs_count_tracks_pmin(xds_kma, dfs, dx=3, dy=3, categ=979,lo0=lo_area[0], lo1=lo_area[1], la0=la_area[0], la1=la_area[1])
dss.to_netcdf(path_p+'xs_dwt_counts3_979_CRs.nc')


# In[55]:


#All categories
ds=xr.open_dataset(path_p+'xs_dwt_counts3_CR.nc')
dsh=xr.open_dataset(path_p+'xs_dwt_counts3_CRh.nc')
dss=xr.open_dataset(path_p+'xs_dwt_counts3_CRs.nc')

#Category >= 2
ds2=xr.open_dataset(path_p+'xs_dwt_counts3_965_CR.nc')
dsh2=xr.open_dataset(path_p+'xs_dwt_counts3_965_CRh.nc')
dss2=xr.open_dataset(path_p+'xs_dwt_counts3_965_CRs.nc')

#Category >= 3
ds3=xr.open_dataset(path_p+'xs_dwt_counts3_979_CR.nc')
dsh3=xr.open_dataset(path_p+'xs_dwt_counts3_979_CRh.nc')
dss3=xr.open_dataset(path_p+'xs_dwt_counts3_979_CRs.nc')


# **Calculation and application of a correction factor to the synthetic data to account for the fact that each year has been simulated more than one time:**

# In[22]:


n_years = 39
n_hist = 171
n_syn = 3900

mean_hist_year = n_hist/n_years
mean_syn_year = n_syn/n_years
n_years_simulated = n_syn/mean_hist_year

factor = n_years_simulated/n_years

print('Period of time: 1979 to 2017')
print('Number of years simulated for the TCs synthetic database: {0}'.format(n_years_simulated))
print('Factor to divide the resultant probabilities from the TCs synthetic database: {0}'.format(factor))


# In[23]:


n_years = 39
n_hist = 171
n_syn = 3900+n_hist

mean_hist_year = n_hist/n_years
mean_syn_year = n_syn/n_years
n_years_simulated = n_syn/mean_hist_year

factor_t = n_years_simulated/n_years

print('Period of time: 1979 to 2017')
print('Number of years simulated for the TCs synthetic database: {0}'.format(n_years_simulated))
print('Factor to divide the resultant probabilities from the TCs synthetic database: {0}'.format(factor_t))


# In[57]:


#All categories
dss = apply_factor(dss,factor)
dst = apply_factor(ds,factor_t)

#Category >= 2
dss2 = apply_factor(dss2,factor)
dst2 = apply_factor(ds2,factor_t)

#Category >= 3
dss3 = apply_factor(dss3,factor)
dst3 = apply_factor(ds3,factor_t)


# In[38]:


xds_timeline = ds_timeline(df,dsh,dsh3,xds_kma)


# In[39]:


mask_bmus_YD, mask_tcs_YD = variables_dwt_super_plot(xds_kma, xds_timeline)


# <br>
# 
# **AWT transferred to the DWTs:**

# In[25]:


n_clusters_AWT = 6
n_clusters_DWT = 49
n_clusters_MJO = 8
fig = Plot_Probs_WT_WT(bmus_AWT, bmus_DWT, n_clusters_AWT, n_clusters_DWT, ttl = 'DWT/AWT',height = 15, width = 3, wt_colors=True)


# <br>
# 
# **Perpetual year**:

# In[41]:


fig = Report_Sim(xds_kma, py_month_ini=1);


# <br> 
# 
# **Chronology during all the calibration period, with the AWT on the left and the TC days included as black dots. DWTs TC genesis activity is proportional to the size of the black dot in the colorbar:**

# In[27]:


fig = Chrono_dwts_tcs(xds_kma, mask_bmus_YD, mask_tcs_YD, awt0_sel_bmus);


# ```{important} 
#     
# **During the TC season months (November, December, January, February, March and April) the DWTs probability is focused on the upper half of the lattice, where most of the TC genesis activity is also located. In the most intense months (January, February and March) DWTs with the highest number of TCs genesis points are especially likely. On the contrary, in the rest of the months, the probability is shared amongst the DWTs of the lower half of the lattice, where there is very few or null TC genesis activity.**
# 
# ```

# ```{important} 
#     
# **Intra annual variability:**
# 
# * Months out of the TCs season: purple, pink, gray and blue -> DWTs from 29 to 49 -> low or null TC activity.
# * Months out of the TCs season: green, orange, red, yellow -> DWTs from 1 to 28 -> high TC activity.
# 
# ```

# ```{important} 
#     
# **Interanual varibility:**
# 
# * All AWTs show high variability in the DWTs probability. For instance, when there is AWT1, is when the TC season ends the latest.
# * Other factors influence such as long-terms trends, maybe associated to SST warming during this time period.  
# 
# ```

# <br />
# <br />
# <br />
# 
# ## Relationship predictor-predictand
# 

# ### DWTs storm frequency and track counting
# 
# 

# **DWTs - TCs tracks according to genesis**

# In[10]:


fig = Plot_DWTs_tracks(16,13,xds_kma, xs, st_bmus, st_lons, st_lats, st_categ, mode='genesis', cocean='lightgray');


# ```{attention} 
# 
# In this case the TCs tracks counting has been done according to the **date of the point of minimum pressure** of the TC track.
# 
# The predictor area is discretized in **squared 3º cells** to compute the storm frequency per DWT.
# 
# ```

# <br>
# 
# **Absolute number of TCs tracks per DWT (Historical database):**

# In[42]:


fig_hist = Plot_DWTs_counts(18,14,dsh, mode='counts_tcs',cfill='grey');


# **Absolute number of TCs tracks per DWT (Synthetic database):**

# In[59]:


fig_syn = Plot_DWTs_counts(18,14,dss, mode='counts_tcs',cfill='grey');


# **Absolute number of TCs tracks per DWT (both databases):**

# In[60]:


fig_total = Plot_DWTs_counts(18,14,dst, mode='counts_tcs',cfill='grey');


# **TCs maximum category reached in the TCs counting cells per DWT (historical database):**

# In[14]:


fig_hist = Plot_DWTs_counts(18,14,dsh, cmap=cat_cmap,mode='tcs_cat',cfill='grey',varmin=0,varmax=5);


# **TCs maximum category reached in the TCs counting cells per DWT (synthetic database):**

# In[61]:


fig_syn = Plot_DWTs_counts(18,14,dss, cmap=cat_cmap,mode='tcs_cat',cfill='grey',varmin=0,varmax=5);


# **TCs maximum category reached in the TCs counting cells per DWT (both databases):**

# In[16]:


fig_total = Plot_DWTs_counts(18,14,dst, cmap=cat_cmap,mode='tcs_cat',cfill='grey',varmin=0,varmax=5);


# <br>
# 
# **Probability of TC conditioned to DWT with the DWT probability as background color (historical database):**

# In[63]:


fig = Plot_DWTs_counts(18,14,dsh, mode='prob_tc');


# <br>
# 
# **Probability of TC>=category 2 conditioned to DWT with the DWT probability as background color (historical database):**

# In[69]:


fig = Plot_DWTs_counts(18,14,dsh2, mode='prob_tc');


# <br>
# 
# **Probability of TC conditioned to DWT with the DWT probability as background color (synthetic database):**

# In[67]:


fig = Plot_DWTs_counts(18,14,dss, mode='prob_tc');


# <br>
# 
# **Probability of TC>=category 2 conditioned to DWT with the DWT probability as background color (synthetic database):**

# In[70]:


fig = Plot_DWTs_counts(18,14,dss2, mode='prob_tc');


# <br>
# 
# **Probability of TC conditioned to DWT with the DWT probability as background color (both databases):**

# In[68]:


fig = Plot_DWTs_counts(18,14,dst, mode='prob_tc');


# <br>
# 
# **Probability of TC>=category 2 conditioned to DWT with the DWT probability as background color (synthetic database):**

# In[72]:


fig = Plot_DWTs_counts(18,14,dst2, mode='prob_tc');


# <br />
# <br />
# 
# ### Calibration time period predictand plotting
# 

# ```{attention} 
# 
# **Recall:**
# 
# * Generation of the index predictor.
# * Clustering of the index predictor in 49 synoptic patterns named DWTs.
# * Calculation of TCs daily probability conditioned to each DWT in 3x3º cells in the target area. 
# 
# **Each day of the calibration period has therefore its TCs probability map infered from its DWT.</u>**
# 
# ```

# <br>

# In[73]:


xds_timeline = ds_timeline(df,dst,dst3,xds_kma)


# <br>
# 
# **This mean expected daily number of TCs in 8x8º cells is aggregated in monthly basis for an easy management and visualization.**

# In[74]:


# resample months
xds_timeM0 = xds_timeline.resample(time='MS', skipna=True).sum()
del xds_timeM0['bmus']
xds_timeM = xds_timeM0.where(xds_timeM0.probs_tcs > 0, np.nan)
#xds_timeM.to_netcdf(path_p+'xds_timeM3_CR.nc')
xds_timeM


# <br>
# 
# **TC probability in 3x3º cells in the target area for the calibration time period (1982-2019), including the historical TC tracks:**

# In[83]:


fig_cali = plot_calibration_period(18,42,xds_timeM,xds_timeline,df,0.06,lo_area,la_area)


# <br>
# 
# **TC reaching category 3 or greater probability in 3x3º cells in the target area for the calibration time period (1982-2019), including the historical TC tracks:**

# In[84]:


fig_cali_cat3 = plot_calibration_period_cat3(18,42,xds_timeM,xds_timeline,df,0.02,8,lo_area,la_area)


# <br>
# 
# **TC probability in 3x3º cells in the target area for year 2015 (El Niño) of the calibration time period (1982-2019) including the historical TC tracks coloured according to their category:**

# In[79]:


fig_year_8 = plot_cali_year(16,16,2015,xds_timeline,xds_timeM,df,35,lo_area,la_area)

