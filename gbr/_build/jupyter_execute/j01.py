#!/usr/bin/env python
# coding: utf-8

# #  Databases

# * **Sea Surface Temperature (SST)**: The NOAA 1/4° daily Optimum Interpolation Sea Surface Temperature or daily OISST is an analysis constructed by combining observations from different platforms (satellites, ships, buoys and Argo floats) on a regular global grid.More information at the OISST NOAA webpage.
# <br>
# 
# * **CFS (Climate Forecast System)**: It is a model produced by several dozen scientists under guidance from the National Centers for Environmental Prediction (NCEP), which offers hourly data with a horizontal resolution down to 1/2º. **Mixed Layer Depth (MLD), Mean Sea Level Pressure (SLP) and forecast data from both SST and MLD.** More information at the CFS NOAA webpage.
# <br>
# 
# * **Daily mean precipitation**: It is  generated from the research-quality 3-hourly **TRMM Multi-Satellite Precipitation** Analysis TMPA (3B42). Simple summation of valid retrievals in a grid cell is applied for the data day. The result is given in (mm). More information at the TRMM NASA webpage or at the TRMM 3B42 dataset product.
# <br>
# 
# * **Madden-Jullian Oscillation (MJO)**: from the Australian Bureau of Meteorology webpage, including MJO date (day, month and year), RMM1, RMM2, phase (1 to 8) and amplitude. More information at the MJO  Australian Bureau of Meteorology.
# <br>
# 
# * **IBTrACS v4**: This global dataset compiles an inventory of tropical cyclones (TCs) reported worldwide with their characteristics (Knapp et al. 2010). Various parameters among which are the best-track position and intensity are provided at 6-h intervals . Data sources are each World Meteorological Organization (WMO) Regional Specialized Meteorological Centers (RSMCs) and Tropical Cyclone Warning Centres (TCWCs), as well as other national agencies.  Data for some basins are available as early as 1850. Data can be accessed and downloaded at the IBTrACS NOAA webpage.
# <br>    
# 
# * **Synthetic TCs provided by Emanuel (for the Great Barrier Reef area)**: The database includes numerous parameters amongst which there is the pressure, the position or the wind velocity at 2-h intervals from 1979 to 2017. Each year of the time period has been simulated an estimated number of 23 times. 
# 
# **Saffir-Simpson classification scale of tropical cyclones. Each category has been assigned a color used for all the figures in the work:**

# ```{figure} ./images/intro2.png
# :name: categories
# 
# Saffir-Simpson classification scale of tropical cyclones.
# ```

# **Historical database (IBTrACS):**

# ```{figure} ./images/intro0.png
# :name: Historical database
# 
# Historical TCs tracks from IBTrACs going through the target area.
# ```

# **Synthetic database (Provided by Emanuel):**
# 

# ```{figure} ./images/intro3.png
# :name: Synthetic database
# 
# Synthetic TCs from the database provided by Emanuel going through the target area.
# ```
