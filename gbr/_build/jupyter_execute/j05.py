#!/usr/bin/env python
# coding: utf-8

# #  Index Predictor Definition and Building

# In[9]:


# basic
import sys
import os

# common
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import pickle

#lib
from lib.plots_base import basemap_ibtracs,basemap_ibtracs_ncep, basemap_var, basemap_scatter, axplot_basemap, basemap_scatter_both, plot_target_area,plot_predictor_grid,plot_target_area_plain
from lib.plots_tcs import get_storm_color, get_category, Plot_DWTs_tracks
from lib.plots_aux import data_to_discret, colors_dwt
from lib.extract_tcs import Extract_Rectangle, dwt_tcs_count, Extract_Rectangle_ncep,Extract_Rectangle_ncep_dataset
from lib.predictor_definition_building import *

import warnings
warnings.filterwarnings('ignore')
from IPython.display import Image


# In[43]:


#path to your daily mean SST and MLD data
path_sst = r'/media/administrador/SAMSUNG/seasonal_forecast/data/SST/'
path_mld = r'/media/administrador/SAMSUNG/seasonal_forecast/data/CFS/ocnmld/'
path_p = r'/home/administrador/Documentos/seasonal/seasonal_forecast/new/'


# <br>
# 
# ## Spatial and temporal domain
# 
# 
# 

# **The predictor area spans around part of the Australian eastern coast, with the Great Barrier Reef as the target zone, from latitude 9º to 26º South and from longitude 140º to 163º West; far enough to be able to identify regional as well as local patterns. The calibration period (time domain) is defined from 1982 to 2019.**

# In[22]:


lo_SP, la_SP = [130,175], [-36,0]

# predictor area
lo_area = [140, 163]
la_area = [-26, -9]


# In[3]:


fig_target_area = plot_target_area_plain(lo_SP,la_SP,rectangle=[lo_area[0], lo_area[1], la_area[0], la_area[1]])


# <br>
# 
# **The variables required for the methodology are downloaded from the databases:**
# 
# + **Predictand**: Tropical cyclones tracks from IBTrACs, for the minimum pressure point.
# 
# + **Predictor**: NOAA 1/4º daily Optimum Interpolation Sea Surface Temperature (SST) and Mixed Layer Depth (MLD) from the NCEP Climate Forecast System Reanalysis (CFSR).
# 
# 

# **For the predictand variable data, two TCs databases are explored, the historical database from IBTrACS Version 4.0 and the synthetic database Version 6.3 from (provided by Emanuel).**

# ### Historical database (IBTrACS)
# 
# 

# In[21]:


# ibtracs v4 dictionary
d_vns_h = {
    'longitude': 'lon',
    'latitude': 'lat',
    'time': 'time',
    'pressure': 'wmo_pres',}


# In[24]:


path_st = r'/home/administrador/Documentos/'
xds_ibtracs, xds_SP = storms_sp(path_st)


# In[50]:


# extract rectangle, 772 a 780
TCs_rect_hist_tracks = Extract_Rectangle(xds_SP, lo_area[0], lo_area[1], la_area[0], la_area[1], d_vns_h) 
TCs_rect_hist_tracks


# In[51]:


# plot
title = 'IBTrACS v4 - SP basin - rectangle ({0})'.format(TCs_rect_hist_tracks.storm.size)
basemap_ibtracs(lo_SP[0], lo_SP[1], la_SP[0], la_SP[1], 
                TCs_rect_hist_tracks, d_vns, title,
               rectangle=[lo_area[0], lo_area[1], la_area[0], la_area[1]]);


# In[26]:


df0 = df_pressures(xds_ibtracs)
df0[6000:6010]


# **For the calibration period the points with pressure, SST and MLD data in the target area are kept.**

# In[51]:


df = df_p_sst_mld(df0,path_sst,path_mld,lo_area[0], lo_area[1], la_area[0], la_area[1])


# In[44]:


dfh = dfh.drop(df.index[2846:]) #years of the calibration period
#dfh.to_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR.pkl')
dfh


# <br>
# 
# ### Synthetic database (Provided by Emanuel)
# 
# 

#   **Original database:**

# In[3]:


ds = xr.open_dataset(r'/home/administrador/Documentos/seasonal/ncep_tracks.nc')
ds


# In[23]:


# ibtracs v4 dictionary
d_vns_s = {
    'longitude': 'longstore',
    'latitude': 'latstore',
    'pressure': 'pstore',}


# In[19]:


tc = Extract_Rectangle_ncep(ds, lo_area[0], lo_area[1], la_area[0], la_area[1], d_vns_s) 


# In[204]:


# plot
title = 'IBTrACS v4 - SP basin - rectangle ({0})'.format(tc.storm.size)
basemap_ibtracs_ncep(list_k,lo_SP[0], lo_SP[1], la_SP[0], la_SP[1], 
                tc, d_vns, title,
               rectangle=[lo_area[0], lo_area[1], la_area[0], la_area[1]]);


# In[308]:


df0 = df_pressures_ncep(tc,list_k,d_vns)
#df0.to_netcdf(path_p+'df_coordinates_pmin_sst_mld_2019_CR_ncep_original.pkl')


# In[30]:


df0 = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_ncep_original.pkl')
df0


# **Prepocessing of the original synthetic database to have the same format and structure of the IBTrACs database.**
# 

# In[32]:


ds_syn = ncep_dataset(ds, df0, lo_area[0], lo_area[1], la_area[0], la_area[1], d_vns) 
ds_syn.to_netcdf(path_p+'ncep.nc')


# **Resultant database:**

# In[16]:


ds_syn = xr.open_dataset(path_p+'ncep.nc')
ds_syn


# <br>
# 
# **For the calibration period the points with pressure, SST and MLD data in the target area are kept. Although there are clear and signficant differences between both databases, as it could be observed when exploring the relationship between the large-scale predictors (AWT and MJO) and the TC activity and the TCs categories and trajectories.**

# In[316]:


dfs = df_p_sst_mld(dfn,path_sst,path_mld,lo_area[0], lo_area[1], la_area[0], la_area[1])
dfs.to_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_ncep.pkl')


# In[37]:


dfs = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_ncep.pkl')
dfs


# <br>
# 
# **Joint dataset of both synthetic and historical databases, SST and MLD data in the target area are kept.**

# In[69]:


dft = pd.concat([dfh,dfs])
dft.to_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_totalok.pkl')


# In[42]:


dft = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_totalok.pkl')
dft


# **All computed dataframes: from historical data, synthetic data and from joining both databases:**

# In[43]:


# load data
dfh = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR.pkl')
dfs = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_ncep.pkl')
dft = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR_totalok.pkl')


# <br>
# 
# ## Predictor grid and data processing
# 
# 

# 
# **The historical datasets are interpolated into the a 1/2º grid resolution, defining this way the grid for the predictor in the target area.**
# 

# In[45]:


#predictor grid
lop = 139.75
lop2 = 165
lap = -8.25
lap2 = -28
delta = 2


# In[95]:


lo_sel = np.arange(lop, lop2, delta)
la_sel = np.arange(lap, lap2, -delta)


# In[18]:


fig_predictor_grid = plot_predictor_grid(2,lop,lop2,lap,lap2,lo_SP,la_SP)


# <br>
# 
# **MLD, SST and Pmin data plotted after preprocessing (left) and the same data after discretization in intervals of 0.5 m and 0.5ºC for MLD and SST respectively (right):**

# **Historical database**

# In[376]:


plot_sst_mlp_pmin_cali(df)


# **Synthetic database**

# In[96]:


plot_sst_mlp_pmin_cali(df)


# **Both databases**

# In[30]:


plot_sst_mlp_pmin_cali(dft)


# <br>
# 
# ## Index definition and computation
# 
# 

# **The historic datasets are combined into the tailor-made index predictor. It is built from the combination of SST-MLD-Pmin of the coordinate dataset previously generated. For simplicity the index will range from 0 to 1, so the pressure range is rescaled in this interval, accounting for the pressure limits of the record.**
# <br>
# 

# In[39]:


# discretization: 0.5ºC (SST), 5m (MLD)
xx,yy,zz = data_to_discret(df['sst'].values, df['mld'].values, 0.5, 5, df['pres'].values, 15, 32, 0, 175, option='min')

# index function
index = zz
fmin, fmax = np.nanmin(zz), np.nanmax(zz)
index_zz = (fmax - index) / (fmax-fmin)

# remove nan
index_zz[np.isnan(index_zz)] = 0


# <br>

# **The index predictor function:** <br>
#     
# 

# ```{math}
# :label: eq_index
# 
# index_{i} = (P_{max} + P_{i}) / (P_{max} - P_{min})
# ```

# <br>
# 
# **The resultant tailor-made index predcitor plotted in the MLD-SST space:**

# **<u>Historical database (chosen)</u>**

# In[21]:


fig_index = plot_index(xx,yy,zz,index_zz)


# **Synthetic database**

# In[98]:


fig_index_t = plot_index(xx,yy,zz,index_zz)


# **Both databases**

# In[32]:


fig_index_t = plot_index(xx,yy,zz,index_zz)


# <br>
# <br>
# 
# **Final dataset including all the variable and the index predictor values in the predictor grid. It is built based on the index previously obtained. In this case it was decided to use the historical database based tailor-made index predictor.**

# In[41]:


path_slp = r'/media/administrador/SAMSUNG/seasonal_forecast/data/CFS/'
path_pp = r'/home/administrador/Documentos/pratel/'
path_trmm = r'/home/administrador/Documentos/TRMM_daily/'


# In[46]:


xs = ds_index_sst_mld_calibration(path_sst,path_mld,df,lop,lop2,lap,lap2,delta)


# In[47]:


xs_f = ds_index_sst_mld_slp_pp_calibration(path_pp,path_slp,xs,lop,lop2,lap,lap2,delta)


# In[26]:


xs_trmm = ds_trmm(path_trmm,lop,lop2,lap,lap2,delta)

