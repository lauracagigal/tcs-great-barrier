#!/usr/bin/env python
# coding: utf-8

# # ALR - DWTs generation

# **Obtain synthetic DWTs timeseries**
# 
# inputs required: 
#   * Historical DWTs
#   * Historical AWT and IWT
#   * Synthetic timeseries of AWT and IWT
#   
# in this notebook:
#   * Fit the ALR model of DWT based on seasonality,and  AWT and IWT timeseries
#   * Generate *n* simulations of 1000 years of DWTs timeseries

# Simulating sequencing and persistence of synthetic DWTs is accomplished with an autoregressive logistic model (ALR). ALR models are simultaneously able to account for covariates varying at different timescales as well as the autocorrelation of those covariates at different orders (Guanche et al., 2013; Antolinez et al., 2015). In this sense, the AWT, seasonality, IWT, as well as the ordering (transitions between DWTs) and duration (persistence within a DWT) can all be accounted for within a single framework to make a categorical decision of what the weather pattern should be on any given day. Mathematically, the model is represented as:
# 
# 
# &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Prob$(Y_t=i|Y_{t-1},...,Y_{t-e},X_t)$ = 
#  
# &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$= {{\exp{\large (}\beta_{0,i} + \beta_{1,i}\cos \omega t + \beta_{2,i}\sin \omega t + \sum\limits_{j=1}^{3}\beta_{j,i}^{awt} APC_j(t) + \sum\limits_{j=1}^{2}\beta_{j,i}^{iwt} IPC_j(t) + \sum\limits_{j=1}^e Y_{t-j\gamma j,i}{\large )}} \over {\sum\limits_{k=1}^{n_{DWT}} \exp{\large (}\beta_{0,k} + \beta_{1,k}\cos \omega t + \beta_{2,k}\sin \omega t + \sum\limits_{j=1}^{3}\beta_{j,k}^{awt} APC_j(t) + \sum\limits_{j=1}^{2}\beta_{j,k}^{iwt} IPC_j(t) + \sum\limits_{j=1}^e Y_{t-j\gamma j,k}{\large )}}}$;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center>
#  
# &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$\forall i = 1,...,n_{ss}$
#  
# where  $\beta_{1,i}$ and $\beta_{2,i}$ covariates account for the seasonal probabilites of each DWT. Covariates $\beta_{j,k}^{awt} APC_j(t)$ account for each weather type’s probability associated with the leading three principle components used to create the AWTs, covariates $\beta_{j,k}^{iwt} IPC_j(t)$ account for the leading two principle components of the MJO, and $Y_{t-j}$ represents the DWT of the previous j-states, and $\beta_{j,i}$  is the parameter associated with the previous j-state, and the order e corresponds to the number of previous states that influence the actual DWT.
# Each of these covariates was found to be statistically significant by the likelihood ratio (Guanche et al. 2014), where inclusion of a covariate required an improvement in prediction beyond a penalty associated with the added degrees of freedom. An iterative method began with the best univariate model (seasonality) and added each covariate in a pair-wise fashion to determine the next best model (seasonality + $APC_1$), continuing this process until all covariates were added. 
# 
# The model performance is evaluated at the end of the notebook by means of comparison historical and simulated probabilities of occurrence of the 42 DWTs during a perpetual year, the transition probabilities between DWTs and finally seasonal and conditional probabilities of occurrance of DWT to AWT and IWT. 

# In[25]:


#!/usr/bin/env python
# -*- coding: utf-8 -*-

# common
import os
import os.path as op

# pip
import numpy as np
import xarray as xr
import pandas as pd

# DEV: override installed teslakit
import sys
sys.path.insert(0, op.join(os.path.abspath(''), '..'))


import warnings
warnings.filterwarnings('ignore')


# In[ ]:


# teslakit 
from teslakit.database import Database
from teslakit.alr import ALR_WRP
from teslakit.util.time_operations import xds_reindex_daily, xds_common_dates_daily

from teslakit.plotting.estela import Plot_DWTs_Probs
from teslakit.plotting.wts import Plot_Probs_WT_WT, Plot_Probs_WT_WT_anomaly
from teslakit.plotting.waves import Plot_Waves_DWTs


# 
# ## Database and Site parameters

# In[2]:


# --------------------------------------
# Teslakit database

p_data = r'/home/administrador/Documentos/seasonal/seasonal_forecast/tcs-emulator'
db = Database(p_data)

# make new site
db.SetSite('TCs-AUS')


# In[3]:


# --------------------------------------
# load data and set parameters

MJO_fit = db.Load_MJO_hist()           # historical MJO
PCs_all = db.Load_SST_PCA()            # SST PCs (annual)

MJO_sim_all = db.Load_MJO_sim()        # MJO simulations (daily)
PCs_sim_all = db.Load_SST_PCs_sim_d()  # SST PCs simulations (daily)

# ALR fit parameters
alr_num_clusters = 49
alr_markov_order = 3
alr_seasonality = [2, 4, 6]

# ALR simulation
num_sims = 5  # one simulation for each simulated MJO, SST 


# In[4]:


#Aqio hay que cargar los DWT
#KMA_fit = db.Load_ESTELA_KMA()         # ESTELA + TCs Predictor

KMA_fit = xr.open_dataset(r'/home/administrador/Documentos/seasonal/seasonal_forecast/new/kma_model/xds_kma_index_vars_1b_CR.nc')


# In[5]:


KMA_fit


# 
# ## ESTELA Predictor - Autoregressive Logistic Regression Fitting

# In[6]:


# --------------------------------------
# Data used to FIT ALR model and preprocess: 

# KMA: bmus (daily) (use sorted_bmus_storms, add 1 to get 1-42 bmus set)
BMUS_fit = xr.Dataset(
    {
        'bmus':(('time',), KMA_fit['bmus'].values[:] + 1),
    },
    coords = {'time': KMA_fit.time.values[:]}
)


# MJO: rmm1, rmm2 (daily)
print(MJO_fit)

# SST: PCs (annual)
sst_PCs = PCs_all.PCs.values[:]
PCs_fit = xr.Dataset(
    {
        'PC1': (('time',), sst_PCs[:,0]),
        'PC2': (('time',), sst_PCs[:,1]),
        'PC3': (('time',), sst_PCs[:,2]),
    },
    coords = {'time': PCs_all.time.values[:]}
)

# reindex annual data to daily data
PCs_fit = xds_reindex_daily(PCs_fit)
print(PCs_fit)


# In[7]:


# --------------------------------------
# Mount covariates matrix (model fit: BMUS_fit, MJO_fit, PCs_fit)

# covariates_fit dates
d_fit = xds_common_dates_daily([MJO_fit, PCs_fit, BMUS_fit])

# KMA dates
BMUS_fit = BMUS_fit.sel(time = slice(d_fit[0], d_fit[-1]))

# PCs covars 
cov_PCs = PCs_fit.sel(time = slice(d_fit[0], d_fit[-1]))
cov_1 = cov_PCs.PC1.values.reshape(-1,1)
cov_2 = cov_PCs.PC2.values.reshape(-1,1)
cov_3 = cov_PCs.PC3.values.reshape(-1,1)

# MJO covars
cov_MJO = MJO_fit.sel(time = slice(d_fit[0], d_fit[-1]))
cov_4 = cov_MJO.rmm1.values.reshape(-1,1)
cov_5 = cov_MJO.rmm2.values.reshape(-1,1)

# join covars 
cov_T = np.hstack((cov_1, cov_2, cov_3, cov_4, cov_5))

# normalize
cov_norm_fit = (cov_T - cov_T.mean(axis=0)) / cov_T.std(axis=0)
cov_fit = xr.Dataset(
    {
        'cov_norm': (('time','n_covariates'), cov_norm_fit),
        'cov_names': (('n_covariates',), ['PC1','PC2','PC3','MJO1','MJO2']),
    },
    coords = {'time': d_fit}
)
print(cov_fit)


# In[8]:


# --------------------------------------
# Autoregressive Logistic Regression

# model fit: BMUS_fit, cov_fit, num_clusters
# model sim: cov_sim, sim_num, sim_years

# ALR terms
d_terms_settings = {
    'mk_order'  : alr_markov_order,
    'constant' : True,
    'long_term' : False,
    'seasonality': (True, alr_seasonality),
    'covariates': (True, cov_fit),
}

# ALR wrapper
ALRW = ALR_WRP(db.paths.site.ESTELA.alrw)
ALRW.SetFitData(alr_num_clusters, BMUS_fit, d_terms_settings)

# ALR model fitting
ALRW.FitModel(max_iter=50000)


# In[9]:


# Plot model p-values and params
ALRW.Report_Fit()


# 
# ## ESTELA Predictor - Autoregressive Logistic Regression Simulation

# In[10]:


# --------------------------------------
# Prepare Covariates for ALR simulations

# simulation dates
d_sim = xds_common_dates_daily([MJO_sim_all, PCs_sim_all])

# join covariates for all MJO, PCs simulations
l_cov_sims = []
for i in MJO_sim_all.n_sim: 

    # select simulation
    MJO_sim = MJO_sim_all.sel(n_sim=i)
    PCs_sim = PCs_sim_all.sel(n_sim=i)

    # PCs covar 
    cov_PCs = PCs_sim.sel(time = slice(d_sim[0], d_sim[-1]))
    cov_1 = cov_PCs.PC1.values.reshape(-1,1)
    cov_2 = cov_PCs.PC2.values.reshape(-1,1)
    cov_3 = cov_PCs.PC3.values.reshape(-1,1)

    # MJO covars
    cov_MJO = MJO_sim.sel(time = slice(d_sim[0], d_sim[-1]))
    cov_4 = cov_MJO.rmm1.values.reshape(-1,1)
    cov_5 = cov_MJO.rmm2.values.reshape(-1,1)

    # join covars (do not normalize simulation covariates)
    cov_T_sim = np.hstack((cov_1, cov_2, cov_3, cov_4, cov_5))
    cov_sim = xr.Dataset(
        {
            'cov_values': (('time','n_covariates'), cov_T_sim),
        },
        coords = {'time': d_sim}
    )
    l_cov_sims.append(cov_sim)

# use "n_sim" name to join covariates (ALR.Simulate() will recognize it)
cov_sims = xr.concat(l_cov_sims, dim='n_sim')
cov_sims = cov_sims.squeeze()

print(cov_sims)


# In[11]:


# --------------------------------------
# Autoregressive Logistic Regression - simulate 

# launch simulation
xds_alr = ALRW.Simulate(num_sims, d_sim, cov_sims, overfit_filter=True)

# Store Daily Weather Types
DWT_sim = xds_alr.evbmus_sims.to_dataset()
db.Save_ESTELA_DWT_sim(DWT_sim)

print(DWT_sim)


# In[12]:


# show sim report
ALRW.Report_Sim(py_month_ini=6, persistences_table=False);


# ### Seasonality

# **Historical DWTs probabilities**

# In[13]:


# Plot Historical DWTs probabilities (with TCs DWTs)

bmus_fit = KMA_fit.bmus.values[:] + 1
dbmus_fit = KMA_fit.time.values[:]

Plot_DWTs_Probs(bmus_fit, dbmus_fit, alr_num_clusters);


# **Simulated DWTs probabilities**

# In[14]:


# Plot Simulated DWTs probabilities (with TCs DWTs)

bmus_sim = DWT_sim.isel(n_sim=0).evbmus_sims.values[:]
dbmus_sim = DWT_sim.time.values[:]

Plot_DWTs_Probs(bmus_sim, dbmus_sim, alr_num_clusters);


# In[15]:


def Load_AWTs_DWTs_Plots_hist_update(db,KMA_fit):
    'Load data needed for WT-WT Probs plot'

    # historical
    xds_AWT = db.Load_SST_KMA()
    xds_DWT = KMA_fit

    # AWT historical - bmus
    xds_AWT = xr.Dataset(
        {'bmus': (('time',), xds_AWT.bmus.values[:])},
        coords = {'time': xds_AWT.time.values[:]}
    )

    # DWT historical - sorted_bmus_storms
    xds_DWT = xr.Dataset(
        {'bmus': (('time',), xds_DWT.bmus.values[:])},
        coords = {'time': xds_DWT.time.values[:]}
    )

    # reindex AWT to daily dates (year pad to days)
    xds_AWT = xds_reindex_daily(xds_AWT)

    # get common dates
    dc = xds_common_dates_daily([xds_AWT, xds_DWT])
    xds_DWT = xds_DWT.sel(time=slice(dc[0], dc[-1]))
    xds_AWT = xds_AWT.sel(time=slice(dc[0], dc[-1]))

    return xds_AWT, xds_DWT


# ### AWT

# **DWTs probabilities transferred to AWT**

# In[34]:


# Plot AWTs/DWTs Probabilities 

# clusters to plot (no TCs)
n_clusters_AWT = 6
n_clusters_DWT = 49
n_sim = 0  # simulation to plot

# Plot AWTs/DWTs Probs - historical
AWT_hist, DWT_hist = Load_AWTs_DWTs_Plots_hist_update(db,KMA_fit)
#AWT_hist = xr.open_dataset(r'/home/administrador/Documentos/seasonal/seasonal_forecast/tcs-emulator/sites/TCs-AUS/SST/SST_KMA.nc')
AWT_bmus = AWT_hist.bmus.values[:]
DWT_bmus = KMA_fit.bmus.values[:]

Plot_Probs_WT_WT(
    AWT_bmus, DWT_bmus, n_clusters_AWT, n_clusters_DWT,
    wt_colors=True, ttl = 'DWTs Probabilities by AWTs - Historical'
);

# Plot AWTs/DWTs sim - simulated
AWT_sim, DWT_sim = db.Load_AWTs_DWTs_Plots_sim(n_sim=0)
AWT_bmus = AWT_sim.bmus.values[:]
DWT_bmus = DWT_sim.bmus.values[:]

Plot_Probs_WT_WT(
    AWT_bmus, DWT_bmus, n_clusters_AWT, n_clusters_DWT,
    wt_colors=True, ttl = 'DWTs Probabilities by AWTs - Simulation'
);


# **DWTs anomaly probabilities transferred to AWT**

# In[35]:


# plot DWTs conditional probabilities to each AWT, minus mean probabilities

# Plot AWTs/DWTs Probs - historical
#AWT_hist, DWT_hist = db.Load_AWTs_DWTs_Plots_hist()
AWT_bmus = AWT_hist.bmus.values[:]
DWT_bmus = DWT_hist.bmus.values[:]

Plot_Probs_WT_WT_anomaly(
    AWT_bmus, DWT_bmus, n_clusters_AWT, n_clusters_DWT,
    wt_colors=True, ttl = 'DWTs anomaly Probabilities by AWTs - Historical'
);

# Plot AWTs/DWTs sim - simulated
#AWT_sim, DWT_sim = db.Load_AWTs_DWTs_Plots_sim(n_sim=0)
AWT_bmus = AWT_sim.bmus.values[:]
DWT_bmus = DWT_sim.bmus.values[:]

Plot_Probs_WT_WT_anomaly(
    AWT_bmus, DWT_bmus, n_clusters_AWT, n_clusters_DWT,
    wt_colors=True, ttl = 'DWTs anomaly Probabilities by AWTs - Simulation'
);


# <br>
# 
# ### Chronology

# **Chronology of the DWTs, inlcuding AWT**

# In[40]:


from lib.plots_dwts import colorp, custom_colorp,Plot_dwts_colormap,Report_Sim,Chrono_dwts_hist,Chrono_dwts_sim
from lib.calibration import variables_dwt_super_plot_sim, variables_dwt_super_plot,ds_timeline, awt_mjo_ds, bmus_dwt_mjo


# **Historical databases needed**

# In[8]:


path_p = r'/home/administrador/Documentos/seasonal/seasonal_forecast/new/'


# In[26]:


xds_kma = xr.open_dataset(path_p+'kma_model/xds_kma_index_vars_1b_CR.nc')
df = pd.read_pickle(path_p+'df_coordinates_pmin_sst_mld_2019_CR.pkl')#historic

#All categories
ds=xr.open_dataset(path_p+'xs_dwt_counts3_CR.nc')
dsh=xr.open_dataset(path_p+'xs_dwt_counts3_CRh.nc')
dss=xr.open_dataset(path_p+'xs_dwt_counts3_CRs.nc')

#Category >= 3
ds3=xr.open_dataset(path_p+'xs_dwt_counts3_965_CR.nc')
dsh3=xr.open_dataset(path_p+'xs_dwt_counts3_965_CRh.nc')
dss3=xr.open_dataset(path_p+'xs_dwt_counts3_965_CRs.nc')

#Category >= 2
ds2=xr.open_dataset(path_p+'xs_dwt_counts3_979_CR.nc')
dsh2=xr.open_dataset(path_p+'xs_dwt_counts3_979_CRh.nc')
dss2=xr.open_dataset(path_p+'xs_dwt_counts3_979_CRs.nc')


# **DWTs colors from the previously made statistical downscaling for the calibration period:**

# In[10]:


# custom colorbar for index
color_ls = ['white','cyan','cornflowerblue','darkseagreen','olivedrab','gold','darkorange','orangered','red','deeppink','violet','darkorchid','purple','midnightblue']
custom_cmap = custom_colorp(color_ls)

# custom colorbar for index
color_cat = ['green','yellow','darkorange','red','purple','black']
cat_cmap = custom_colorp(color_cat)


# **Resulting DWTs lattice and corresponding colors assigned:**

# In[11]:


fig_dwt_lattice = Plot_dwts_colormap(xds_kma.n_clusters.size)


# **Calculation of the variables to plot**

# In[32]:


path_mjo_awt = '/home/administrador/Documentos/seasonal/seasonal_forecast/'


# In[36]:


awt,mjo,awt0 = awt_mjo_ds(path_mjo_awt)


# In[39]:


bmus_DWT, bmus_time,awt0_sel_bmus,bmus_AWT,bmus_MJO = bmus_dwt_mjo(mjo,awt,awt0,xds_kma)


# In[27]:


xds_timeline = ds_timeline(df,dsh,dsh3,xds_kma)


# In[30]:


mask_bmus_YD, mask_tcs_YD = variables_dwt_super_plot(xds_kma, xds_timeline)


# **Simulated DWTs**

# In[17]:


#dataset with the DWTs simulated
sim = xr.open_dataset(r'/home/administrador/Documentos/seasonal/seasonal_forecast/DWT_sim.nc')

#how many years to plot
n_years = 100
limit = int(365.25*n_years)
sim_plot = sim.isel(time=slice(0,limit))
sim_plot


# **Simulated AWTs**

# In[18]:


#dataset with the AWTs simulated
awt_sim = xr.open_dataset(r'/home/administrador/Documentos/seasonal/seasonal_forecast/tcs-emulator/sites/TCs-AUS/SST/SST_AWT_sim.nc')

awt_sim = awt_sim.isel(time=slice(1,n_years+1))
awt_sim_val = awt_sim.evbmus_sims.values
awt_sim


# In[20]:


#which simulation to plot
n_sim = 1

mask_bmus_YDs = variables_dwt_super_plot_sim(sim_plot,n_years,1)


# **Historical DWTs chronology (calibration time period from 1982 to 2019)**

# In[41]:


fig = Chrono_dwts_hist(xds_kma, mask_bmus_YD,  mask_tcs_YD,awt0_sel_bmus);


# **Simulated DWTs chronology (calibration time period from 1982 to 2019)**
# 

# **For instance, first 100 years of the second simulation:**

# In[42]:


fig = Chrono_dwts_sim(sim_plot, xds_kma, mask_bmus_YDs, mask_tcs_YD, awt_sim_val[:,1],height=18);


# ```{important} 
# 
# **Both historical and simulated DWTs shown similar seasonality and chronology patterns.**
# 
# ```
