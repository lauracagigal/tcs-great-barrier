#!/usr/bin/env python
# coding: utf-8

# #  Large Scale Predictors
# 

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
from lib.large_scale_predictors_functions import *
path = r'D:\\forecast_book\\large_scale_predictor_data\\'
path_data = 'C:\\Users\\Andrea\\TRABAJO\\coral_reef\\'
from mpl_toolkits.basemap import Basemap

import warnings
warnings.filterwarnings('ignore')


# In[2]:


#TC historical tracks
tc = xr.open_dataset(path_data +'cr_tcs_1979_2019.nc')
en = xr.open_dataset(path_data+'ncep.nc')
#mjo and awt
mjo_awt = xr.open_dataset(path_data + 'mjo_awt_new.nc')


# In[3]:


lo_area = [140,163]
la_area = [-26,-9]


# ##  Madden - Julian Oscillation (MJO)
# 

# The MJO is an eastward moving disturbance of clouds, rainfall, winds, and pressure that crosses the planet in the tropics and returns to its initial starting point in cycles of approximately 30 or 60 days. It is the dominant mode of atmospheric intraseasonal variability in the tropics (Hendon & Salby, 1994): 
# <br>
# 
# The MJO consists of two phases: **the enhanced rainfall and the the suppressed rainfall**. They produce opposite changes in clouds and rainfall and this entire dipole propagates eastward. Strongest MJO activity often divides the planet into halves: one half within the enhanced convective phase and the other half in the suppressed convective phase.
# <br>
# 
# The MJO phases can be observed and represented through the **RMM index**, which is a combined cloudiness- and circulation-based index that has been frequently used for real-time prediction and definition of the MJO (Wheeler & Hendon, 2004): 
# <br>

# ```{figure} ./images/mjo1.png
# :name: mjo
# 
# Phase-space diagram of the RMM index showing daily phase (quadrant) and magnitude (distance from center) of the MJO from 1 Jun 1974 through 31 Mar 2014. Colors indicate thresholds of activity: IA (blue, RMM < 1.0), A (green, RMM ≥ 1.0 and < 1.5), VA (brown, RMM ≥ 1.5 and < 2.5), and EA (red, RMM ≥ 2.5) (Source: Wheeler & Hendon, 2004).
# 
# ```

# <br>
# 
# ##  Annual Weather type (AWT)
# 

# ENSO is one of the most important climate phenomena on Earth due to its ability to change the global atmospheric circulation; since it can lead to changes in sea-level pressures, sea-surface temperatures, precipitation and winds across the globe. **ENSO describes the natural interannual variations in the ocean and atmosphere in the tropical Pacific**. This interaction between the atmosphere and ocean is the source of a periodic variation between below-normal and above-normal sea surface temperatures and dry and wet conditions along the years. The tropical ocean affects the atmosphere above it and the atmosphere influences the ocean below it.
# <br>
# 
# In the figure bellow it can be seen the typical behavior of the couple system of ocean and atmosphere during El Niño in the equatorial Pacific: 
# <br>
# 
# 

# ```{figure} ./images/awt0.jpg
# :name: niño
# 
# Typical behavior of the couple system of ocean and atmosphere during El Niño, conditions in the equatorial Pacific (Source: Australian Bureau of Meteorology webpage).
# 
# ```

# SST pattern and numeration and color assigned for the AWTs used based on the ENSO (Anderson,et al.,2019):

# ```{figure} ./images/awts.png
# :name: awt
# 
# SST patterns for each AWT and the corresponding number and color assigned to each one (Anderson,et al.,2019).
# 
# 
# ```

# <br>
# <br>

# 
# ## Historical vs synthetic database

# ### Preliminary analysis: TCs trajectories and categories
# 

# **Histogram of TCs maximum category reached**

# In[4]:


fig_cat = plot_hist_cat(en,tc)


# **Tracks Trajectories:**

# In[4]:


y=1991

var='wmo_pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,tc,lo_area,la_area,var)

var='pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,en,lo_area,la_area,var)


# In[8]:


y=1997

var='wmo_pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,tc,lo_area,la_area,var)

var='pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,en,lo_area,la_area,var)


# In[9]:


y=2014

var='wmo_pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,tc,lo_area,la_area,var)

var='pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,en,lo_area,la_area,var)


# <br>
# 
# ### Month of the year
# 

# **Histogram of TC genesis according to month of the year**

# In[4]:


fig_month = plot_hist_month(en,tc)


# <br>
# 
# ### AWT

# **Histograms of TC genesis according to AWT**

# **Historical database**

# In[5]:


hist_awts = plot_hist_awt(tc)


# **Synthetic database**

# In[6]:


hist_awts = plot_hist_awt(en)


# <br>
# 
# ### MJO
# 

# **Histograms of TC genesis according to MJO phase**

# **Historical database**

# In[7]:


hist_mjo = plot_hist_mjo(mjo_awt,tc)


# **Synthetic database**

# In[4]:


hist_mjo = plot_hist_mjo(mjo_awt,en)


# <br>
# 
# ### MJO and AWT
# 

# **TC genesis according to AWT and MJO phase**

# **Historical database**

# In[8]:


fig_awt_mjo = plot_hist_mjo_awt(mjo_awt,tc)


# **Synthetic database**

# In[9]:


fig_awt_mjo = plot_hist_mjo_awt(mjo_awt,en)


# <br>
# 
# ## MJO and AWT relationship with TC genesis

# **TCs tracks transferred to MJO+AWT combinations according to the genesis point, with the combination probability as the background color**

# **Historical database:**

# In[4]:


fig_tcs = plot_tcs_mjo_awt(20,12,mjo_awt,tc,lo_area,la_area,'wmo_pres')


# **Synthetic database:**

# In[5]:


fig_tcs = plot_tcs_mjo_awt(20,12,mjo_awt,en,lo_area,la_area,'pres')


# In[4]:


y=1991

var='wmo_pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,tc,lo_area,la_area,var)

var='pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,en,lo_area,la_area,var)


# In[8]:


y=1997

var='wmo_pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,tc,lo_area,la_area,var)

var='pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,en,lo_area,la_area,var)


# In[9]:


y=2014

var='wmo_pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,tc,lo_area,la_area,var)

var='pres'
fig_2015_2016 = plot_season(y,mjo_awt,22,4,en,lo_area,la_area,var)


# <br>
# 
# ## MJO and AWT relationship with TC genesis and daily mean precipitation

# In[4]:


path_trmm = r'D:\\TRMM\\'


# In[5]:


lo1_trmm = 139.75
lo2_trmm = 164.25
la1_trmm = -8.75
la2_trmm = -27.25


# In[7]:


xs_trmm = ds_trmm(path_trmm,lo1_trmm,lo2_trmm,la1_trmm,la2_trmm,2)


# In[6]:


xs_trmm = xr.open_dataset(path_trmm+'trmm_cr.nc')


# <br>
# 
# **Daily mean precipitation (TRMM) transferred to MJO+AWT combinations**

# In[7]:


fig_trmm = plot_trmm(20,12,mjo_awt,xs_trmm,tc,lo_area,la_area)


# <br>
# 
# **Daily mean precipitation (TRMM) anomalies transferred to MJO+AWT combinations**

# In[7]:


fig_anom_trmm = plot_trmm_anom(20,12,mjo_awt,xs_trmm,tc,lo_area,la_area,m=12)


# ```{important} 
# 
# **Conclusions:**
# 
# * MJO phases 5,6 and 7 and AWT 1 and 4 show the highest TC genesis activity.
# * El Niño, which is very unlikely has fewer TC genesis but it is the very active in TCs genesis with respect of its total days comparing to the rest of AWTs.
# * AWT 3 is the least probable and is the one with least TCs genesis activity.
# * TCs genesis occurs generally in areas of intense precipitation, above the mean (positive anomalies).
# 
# ```

# In[ ]:




